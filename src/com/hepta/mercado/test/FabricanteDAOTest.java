/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.test;

import org.junit.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.FabricanteDAO;

/**
 * Implementação dos testes referentes a classe de négocio {@link Fabricante}.
 * 
 * @author jose Orlando
 */
public class FabricanteDAOTest {

	FabricanteDAO fabricantedao = new FabricanteDAO();

	@Test
	public void salvar() throws Exception {
		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Tio Jorge");
		
		fabricantedao.save(fabricante);
	}

}
