/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.test;

import org.junit.Ignore;
import org.junit.Test;

import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.ProdutoDAO;

/**
 * Implementação dos testes referentes a classe de négocio {@link Produto}.
 * 
 * @author jose Orlando
 */
public class ProdutoDAOTest {
	ProdutoDAO produtoDao = new ProdutoDAO();
			
	@Test
	@Ignore
	public void salvar(){
		try {
			Produto produto = new Produto();
			produto.setNome("Arroz");
			produto.setUnidade("Kg");
			produto.setVolume(1.0);
			produto.setEstoque(50);
			produtoDao.save(produto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void findById() throws Exception {
		Produto produto2 = produtoDao.find(8);
		
		produto2.setNome("Açucar");
		produto2.setUnidade("Kg");
		produto2.setVolume(1.0);
		produto2.setEstoque(30);
		
		produtoDao.update(produto2);
		
	}

}
