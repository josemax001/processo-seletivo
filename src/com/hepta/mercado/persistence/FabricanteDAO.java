/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.persistence;

import javax.persistence.EntityManager;

import com.hepta.mercado.entity.Fabricante;

/**
 * Implementação dos testes referentes a classe de négocio {@link Fabricante}.
 * 
 * @author jose Orlando
 */
public class FabricanteDAO {

	/**
	 * Inclui um {@link Fabricante} na Base de dados.
	 * 
	 * @param produto
	 * @throws Exception
	 */
	public void save(Fabricante fabricante) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(fabricante);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
}
