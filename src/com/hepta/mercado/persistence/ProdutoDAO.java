/*
 * ProdutoDAO.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.mercado.entity.Produto;

/**
 * Classe de Persistência de '{@link Produto}'.
 *
 * @author jose Orlando.
 */
public class ProdutoDAO {

	/**
	 * Inclui um {@link Produto} na Base de dados.
	 * 
	 * @param produto
	 * @throws Exception
	 */
	public void save(Produto produto) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(produto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	/**
	 * Atualiza um {@link Produto} na Base de dados.
	 * 
	 * @param produto
	 * @return
	 * @throws Exception
	 */
	public Produto update(Produto produto) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Produto produtoAtualizado = null;
		try {
			em.getTransaction().begin();
			produtoAtualizado = em.merge(produto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produtoAtualizado;
	}

	/**
	 * Deleta um {@link Produto} na Base de dados.
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void delete(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			Produto produto = em.find(Produto.class, id);
			em.remove(produto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	/**
	 * Retorna um {@link Produto} pelo Id cadastrado na Base de dados.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Produto find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Produto produto = null;
		try {
			produto = em.find(Produto.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produto;
	}

	/**
	 * Retorna todos os {@link Produto} da Base de dados.
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Produto> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Produto> produtos = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Produto");
			produtos = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produtos;
	}

}
